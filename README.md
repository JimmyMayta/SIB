# SIB

Proyecto open source


Para descargar o clonar el proyecto:
<https://gitlab.com/JimmyMayta/SIB>

    jimmy@JM:~/Escritorio$ git clone https://gitlab.com/JimmyMayta/SIB.git

Para iniciar el proyecto se debe instalar las dependencias adecuadas

Comando: instalar dependencias

    jimmy@JM:~/ProjectsJavaScript/SIB$ npm install

Comando: iniciar proyecto

    jimmy@JM:~/ProjectsJavaScript/SIB$ DEBUG=sib:* npm start

Comando: iniciar proyecto (Windows)

    C:\Users\Jimmy Mayta\Desktop\SIB> npm start

Desplegar proyecto:

<localhost:3000>

Actualización: <https://gitlab.com/JimmyMayta/SIB-LaPaz>



